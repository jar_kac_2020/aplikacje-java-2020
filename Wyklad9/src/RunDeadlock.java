public class RunDeadlock {
    static class Osoba {
        private final String imie;
        public Osoba(String imie) {
            this.imie = imie;
        }
        public synchronized void usciskaj(Osoba drugaOsoba) {
        	System.out.format( "%s->%s\n", this.imie, drugaOsoba.imie);
            drugaOsoba.sciskam(this);
        }
        public synchronized void sciskam(Osoba drugaOsoba) {
        	System.out.format( "%s<-%s\n", drugaOsoba.imie, this.imie);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        final Osoba o1 = new Osoba("Ala");
        final Osoba o2 = new Osoba("Ola");
        Thread t1 = new Thread(()-> o1.usciskaj(o2) );
        Thread t2 = new Thread(()-> o2.usciskaj(o1) );
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println("Koniec");
    }
}