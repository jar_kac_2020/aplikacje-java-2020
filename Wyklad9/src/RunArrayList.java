import java.util.ArrayList;
import java.util.List;

public class RunArrayList {
	static List<Integer> lista = new ArrayList<>();
	public static void main(String[] args) throws InterruptedException {
		Thread t = new Thread( ()->{
			for (int i=0;i<10000;i++) synchronized(lista) {
				lista.add( Integer.valueOf(i) );
			}
		} );
		t.start();
		for (int i=0;i<10000;i++) synchronized(lista) {
			lista.add( Integer.valueOf(i) );
		}
		t.join();
		System.out.println( lista.size() );
	}
}
