import java.util.*;
import java.util.stream.*;

class PlusPlusI extends Thread {
	long i = 0;
	public void run() {
		while (!Thread.interrupted()) {
			i++;
		}
	}
}

public class RunPriority {
	private static final int THREAD_COUNT = 10;

	public static void main(String[] args) throws InterruptedException {
		final PlusPlusI t[] = new PlusPlusI[THREAD_COUNT];
		List<Integer> indexes = IntStream.range(0,THREAD_COUNT-1).
				mapToObj(i->Integer.valueOf(i)).collect(Collectors.toList());
		indexes.forEach( i->{
			t[i] = new PlusPlusI();
			t[i].start();
			t[i].setPriority(Thread.MIN_PRIORITY+i);
		});
		Thread.sleep(10000);
		Collections.shuffle(indexes);
		indexes.forEach( i->t[i].interrupt() );
		Collections.shuffle(indexes);
		indexes.forEach( i->{try { t[i].join(); } catch (Exception e) {}} );
		Collections.sort(indexes);
		indexes.forEach( i->System.out.println( "Priorytet "+t[i].getPriority()+": "+t[i].i ) );
	}
}
