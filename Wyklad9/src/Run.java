import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

class  PlusPlus {
	public int x=0;
	public void powieksz() {
		x++; 
	}
}

class  Dodaj extends Thread {
	PlusPlus p;
	Dodaj( PlusPlus r ) { p = r; }
	public void run() {
		synchronized (p) {
			int i=0; for (;i<50000000;i++) {
				p.powieksz(); }
			System.out.println( "Wykonano razy: "+i );
		}
	}
}

public class Run {
	public static void main(String[] x ) throws InterruptedException, ExecutionException {
		PlusPlus p = new PlusPlus();
		Dodaj t1, t2;
		ExecutorService ex = Executors.newFixedThreadPool(2);
		Future<String> wynik = ex.submit( ()->{
			Thread.sleep(1000);
			return "123";
		} );
		while (true) {
			if (wynik.isDone()) {
				System.out.println( wynik.get() );
				break;
			}
			Thread.sleep(100);
			System.out.print(".");
		}
		ex.shutdown();
	}
}
