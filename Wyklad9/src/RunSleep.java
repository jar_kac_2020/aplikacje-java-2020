import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RunSleep {
	static List<String> lista = new ArrayList<>();

	public static void main(String[] args) throws IOException, InterruptedException {
		Thread licznik = new Thread(()->{
			try {
				while (true) {
					int ileZnakow = lista.stream().collect( Collectors.summingInt(String::length));
					System.out.println(ileZnakow);
					if (Thread.interrupted())
						throw new InterruptedException();
					Thread.sleep(3000);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});
		licznik.start();

		BufferedReader br = new BufferedReader( new InputStreamReader( System.in ));
		String linia;
		while ( ((linia = br.readLine())!=null)) {
			if (linia.isEmpty()) {
//				licznik.interrupt();
				break;
			}
			lista.add(linia);
		}
		System.out.println("Koniec, czekamy na zakonczenie licznika");
		licznik.join(1000);
		System.out.println("Koniec.");
	}
}
