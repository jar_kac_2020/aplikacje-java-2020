
public class ManagerSterownikow {
	
	private static ManagerSterownikow instance = null;
	
	private ManagerSterownikow() {
		System.out.println( "Utworzony obiekt klasy ManagerSterownikow");
	}
	
	public static ManagerSterownikow getInstance() {
		if (instance == null) {
			instance = new ManagerSterownikow();
		}
		return instance;
	}

	public void register(Class class1) {
		System.out.println("Zarejestrowano "+class1);
		
	}

}
