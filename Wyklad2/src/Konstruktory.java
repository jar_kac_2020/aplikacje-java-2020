
public class Konstruktory extends Object {
	public static int elemStat;
	public static final int X = 5;
	private int poleNieStatyczne;
	private int liczba;
	
	static {
		ManagerSterownikow.getInstance().register( Konstruktory.class );
		ManagerSterownikow m = ManagerSterownikow.getInstance();
	}
	
	public static void metodaS1() {
		
	}
	
	public final void metodaF() {
		
	}
	
	public static void metodaS() {
		final int i;
		
		i = 5;
		elemStat = 5;
		metodaS1();
		
	}
	
	public Konstruktory(int n, String l, boolean b) {
		this.liczba = n;
		System.out.println( "Dlugie i skomplkowane operacje: "+n+l+b );
		System.out.println( "To jest obiekt nr "+elemStat++);
	}
	
	public Konstruktory(int n, String l) {
		this( n, l, false );
	}

	public Konstruktory(int n) {
		this( n, "(ktos)", false );
	}

	public Konstruktory(String l, String b) {
		this( 0, l+b, false );
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + liczba;
		result = prime * result + poleNieStatyczne;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
//		if (getClass() != obj.getClass())
//			return false;
		if (!(obj instanceof Konstruktory)) return false;
		Konstruktory other = (Konstruktory) obj;
		if (liczba != other.liczba)
			return false;
		if (poleNieStatyczne != other.poleNieStatyczne)
			return false;
		return true;
	}
	
//	@Override
//	public boolean equals(Object obj) {
//		if (obj == null) return false;
//		if (obj == this) return true;
//		if (!(obj instanceof Konstruktory)) return false;
//		Konstruktory other = (Konstruktory)obj;
//		if (other.liczba == this.liczba) return true;
//		return false;
//	}
	
	
}
