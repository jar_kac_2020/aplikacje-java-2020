import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

public class KonstruktoryTest {

	@Test
	public void test() {
		Konstruktory.elemStat = 1;
		Konstruktory.metodaS();

		Konstruktory k = new Konstruktory( 5, "Kowalski", true );
		k.elemStat = 5;

		new Konstruktory( 7, "Kowalski", true );
		new DziedziczyZKonstruktory();

		Gromada g = Gromada.GADY;
		g = Gromada.valueOf( "SSAKI" );
		Gromada[] gromady = Gromada.values();
		for (Gromada gromada:gromady) {
			System.out.println( gromada.toString() );
		}

		String b = "12";
		b += "3";
		if (b.equals("123")) {
			System.out.println("TRUE");
		} else {
			System.out.println("FALSE");
		}

		Konstruktory k1 = new Konstruktory( 7, "Kowalski", true );
		Konstruktory k2 = new DziedziczyZKonstruktory( 7 );
		System.out.println( "Czy to samo? "+(k1.equals( k2 )));
		int a1[][] = { {1,2}, {3,4} };
		int a2[][] = { {1,2}, {3,4} };
		assertArrayEquals( a1, a2 );
//		assertTrue( 		Arrays.deepEquals(a1, a2) );
		
	}

}
