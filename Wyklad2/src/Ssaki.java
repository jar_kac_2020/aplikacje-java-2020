
public enum Ssaki {
	KOT(0), PIES(1);
	
	private int idx;

	private Ssaki( int i) {
		this.idx = i;
	}
	
	public int getIdx() {
		return idx;
	}
}
