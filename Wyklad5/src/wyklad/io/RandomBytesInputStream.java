package wyklad.io;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

public class RandomBytesInputStream extends InputStream {

	Random r = new Random();
	
	@Override
	public int read() throws IOException {
		// TODO Auto-generated method stub
		return r.nextInt(256);
	}

}
