package wyklad.io;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class Run {

	public static void main(String[] args) throws IOException {
		byte buf[] = { 1, 3,6,12,4,2,4,7,3,1,0,17 };
		buf = "abćd".getBytes();
		InputStream rbis = new ByteArrayInputStream(buf);
		
		System.out.println( "Available: "+rbis.available() );
		for (int i = 0; i < 10; i++) {
			try {
				System.out.println( rbis.read()+" (available: "+rbis.available()+")" );
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		int znak;
		while ( (znak=rbis.read()) != -1 ) {
			
		}
		
		System.out.println( "Available: "+rbis.available() );
		
		rbis.reset();
		Reader r = new InputStreamReader( rbis );
		while ((znak = r.read())!=-1) {
			System.out.println( (char)znak );
		}
		
		BufferedReader br = new BufferedReader( new InputStreamReader( System.in ) );
		String linia = br.readLine();
		
		try (
				BufferedReader br1 = new BufferedReader( new InputStreamReader( new FileInputStream("plik.txt")  ) );
				Reader r1 = new InputStreamReader( rbis );
				
				) {
			br1.read();
			r1.read();
		} 
		finally {
			
		}
		rbis.close();
	}

}
