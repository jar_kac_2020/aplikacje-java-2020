package wyklad.io.buffered;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class Run {
	public static void main(String[] args) throws IOException {
		String plik = "/Users/jarekk/Desktop/java-w/plik";
		long t0 = System.currentTimeMillis();
		InputStream is = new BufferedInputStream( 
				new FileInputStream(plik), 64
			);
		is.readAllBytes();
//		while (is.available()>0) is.read();
		is.close();
		long t1 = System.currentTimeMillis();
		System.out.println( t1-t0 );
	}
}
