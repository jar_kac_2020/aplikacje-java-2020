import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Run {

	public static void main(String[] args)  {
		try {
			new X(-1);
		} catch (OurException e) {
			e.printStackTrace();
		}
		
		metoda1(); 
		System.out.println("po wyjatku (main)");
	}


	private static void metoda1() {
		try {
			metoda2(); 
			System.out.println("po wyjatku (1)");
		}
		catch( ArrayIndexOutOfBoundsException e ) {
			System.out.println( e.getMessage() );
			return;
		}
		catch( NullPointerException | IOException e ) {
			System.out.println( e.getMessage() );
			return;
		} 
		finally {
			System.out.println( "Finally" );
		}
		System.out.println("po try/catch");
	}


	private static void metoda2() throws IOException {
		int x[] = {1,2,3};
		int znakZKlawiatry = System.in.read();
		System.out.println(x[znakZKlawiatry]); // throw
		((String)null).charAt(0);
		System.out.println("po wyjatku (2)");
	}


	public static void main2(String[] args) throws OurException {
		X a = new X(5);
		X b = new X(5);
		Map<X, String> mapa = new HashMap<>();
		mapa.put( a, "A");
		mapa.put( b, "B");
		for (X x:mapa.keySet()) {
			System.out.println("Klucz "+x);
		}

		List<X> lista = new LinkedList<>();

	}

	public static void main1(String[] args) {
		String tekst = "93847@edu.p.lodz.pl, ala@gmail.com, ela@gmail.com, ula@gmail";
		Pattern p = Pattern.compile("([^a][0-9a-z]+)@((?:[a-z]+\\.)+[a-z]+)");
		Matcher m = p.matcher(tekst);
		while (m.find()) {
			System.out.println( m.group()+" Uzytkownik = "+m.group(1)+
					", serwer="+m.group(2) );
		}
		System.out.println(tekst.substring(1,3));


	}
}
