import java.security.InvalidParameterException;

public class X {
	private int x;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		X other = (X) obj;
		if (x == other.x)
			return false;
		return true;
	}

	public X( int x ) throws OurException {
		this.x = x;
		if (x<0) throw new OurException();
	}
}
