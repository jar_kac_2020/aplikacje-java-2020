import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

public class Run {
	int minDlug = 40;
	long ile = 0;
	
	public static void main(String[] args) {
		(new Run()).metoda();
	}
	
	public void metoda() {
//		StringToUpperCase stuc = (s)->s.toUpperCase();

//		IntStream intStream = Arrays.stream( new int[] { 5, 11, 15 } );
//		OptionalInt suma = intStream.reduce((a,b)->{
//			System.out.println( "a = "+a+", b = "+b);
//			return a+b;
//		} );
//		System.out.println("Wynik = "+suma );
//		
//		if (1==1) return;
		
		
		Stream<String> s = Arrays.stream( new String[] {"a", "b", "bb", "bbb", "bbbb", "c"} );
		s = s.filter( e->e.charAt(0)=='b');
		s = s.skip(1);

//		System.out.println( s.collect( Collectors.joining(", ") ) );
		Map<Boolean, List<String>> mapa = s.collect( Collectors.partitioningBy( e->e.length()>3) );
//		System.out.println( mapa.get( Boolean.TRUE ) );
		mapa.values().stream()
		.flatMap( Collection::stream )
		.forEach(System.out::println);
		
//		Optional<String> elementPoFiltrowaniu = liczbyLosoweZOpisem
//				.distinct()
//				.map( s->s+"!" )
//				.map( s->{ 
//					ile++;
//					if (ile%100000==0) System.out.println(ile);
//					return s; 
//				} )
//				.filter( t->t.length()>minDlug )
//				.findFirst();
//		
//		if (elementPoFiltrowaniu.isPresent()) {
//			System.out.println( elementPoFiltrowaniu.get() );
//		}
	}
}
