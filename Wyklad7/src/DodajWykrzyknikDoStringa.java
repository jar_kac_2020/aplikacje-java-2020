import java.util.function.Function;

public class DodajWykrzyknikDoStringa implements Function<String, String> {
	public String apply( String wejscie ) {
		System.out.println("DodajWykrzyknikDoStringa.apply("+wejscie+") ["+Thread.currentThread().getId()+"]");
		return wejscie + "!";
	}
}
