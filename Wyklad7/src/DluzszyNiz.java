import java.util.function.Predicate;

public class DluzszyNiz implements Predicate<String> {

	private int i;

	public DluzszyNiz(int i) {
		this.i = i;
	}

	@Override
	public boolean test(String t) {
		return t.length()>i;
	}

}
