
public class Czlowiek {
	private static final int OD_KIEDY_MOZNA_KUPIC_PIWO = 18;
	
	int wiek;
	String imie;

	private Czlowiek przyjaciel;
	
	public boolean czyJestesPelnoletni() {
		return wiek>=OD_KIEDY_MOZNA_KUPIC_PIWO;
	}
	
	public void przedstawSie() {
		System.out.println( "Czesc, jestem "+imie );
	}
	
	public Czlowiek( String imie, int wiek) {
		this.imie = imie;
		this.wiek = wiek;
	}
	
	public void ustawNajlepszegoPrzyjaciela( Czlowiek p ) {
		this.przyjaciel = p;
	}
	
	public Czlowiek ktoJestTwoimPrzyjacielem() {
		return przyjaciel;
	}
}
