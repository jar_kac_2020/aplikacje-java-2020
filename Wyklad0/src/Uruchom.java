public class Uruchom {
	public static void main(String[] args) {
//		Test t;
//		t = new Test( 5 );
//		System.out.println( "W tescie jest "+t.dajIloscPytan()+" pytan. Masz 10sek.");
//		Test t2 = new Test(99);
//		System.out.println( "W tescie zaliczeniowym jest "+t2.dajIloscPytan()+" pytan. Masz 10sek.");

		Czlowiek c; // Nadklasa, Superclass
		c = new Czlowiek( "Ala", 15 );
		c.przedstawSie();
		if (c.czyJestesPelnoletni()) {
			System.out.println("Moge kupic piwo.");
		}
		
		Student s = new Student( "Ola", 19 ); // Podklasa, Subclass
		s.przedstawSie();
		s.wypijPiwo();

		c = s;
		c.przedstawSie();
		s.przedstawSie();

		
		StudentWydzialuElektrycznego se;
		
		s = (Student)c;
		c.przedstawSie();

		s.ustawNajlepszegoPrzyjaciela( c );
		System.out.println( s.ktoJestTwoimPrzyjacielem() );
	}
	
}
