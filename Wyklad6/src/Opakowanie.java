
public class Opakowanie<T extends Number> implements CzyPusty<T>, Comparable<Opakowanie<? extends Number>>{

	private T obj;
	private String s;
	
	public void zapakuj(T o) {
		obj = o;
	}
	
	public T daj() {
		return obj;
	}

	@Override
	public boolean czyJestWSrodkuCos() {
		return obj != null;
	}

	@Override
	public boolean czyWSrodkuJestObiekt(T t) {
		return czyJestWSrodkuCos() && obj.equals(t);
	}
	
	@Override
	public String toString() {
		return obj.toString();
	}

	@Override
	public int compareTo(Opakowanie<? extends Number> o) {
		return (int)(obj.doubleValue()-o.daj().doubleValue());
	}
	
	
}
