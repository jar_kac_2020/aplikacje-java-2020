
public interface CzyPusty<T> {
	public boolean czyJestWSrodkuCos();
	public boolean czyWSrodkuJestObiekt(T t);
}
