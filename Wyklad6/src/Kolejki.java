import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class Kolejki {
	public static void main(String[] args) {
		Queue<String> q = new PriorityQueue<>( new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o1.length()-o2.length();
			}
		});
		q.add( "Aaaa" );
		q.add( "Bbbbbbb" );
		q.add( "Cc" );
		Iterator<String> it = q.iterator();
		while (it.hasNext()) {
			System.out.println( it.next() );
		}
	}
}
