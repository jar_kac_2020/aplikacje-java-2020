import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class Run {

	public static void main(String[] args) {
		Iterable i;
		Opakowanie<Integer> o = new Opakowanie<>();
		Number n = Integer.valueOf(5);

		o.zapakuj( 123 );
//		System.out.println( o.daj() );

		List<Opakowanie<Integer>> lista = new ArrayList<>();
		lista.add( o );
		lista.add( new Opakowanie<Integer>() { { this.zapakuj(5);} } );
		lista.add( new Opakowanie<Integer>() { { this.zapakuj(0);} } );

		lista.sort( new Comparator<Opakowanie<Integer>>() {
			@Override
			public int compare(Opakowanie<Integer> o1, Opakowanie<Integer> o2) {
				return o1.compareTo(o2);
			}
			
		});
		Collections.sort(lista, Collections.reverseOrder(new Comparator<Opakowanie<Integer>>() {
			@Override
			public int compare(Opakowanie<Integer> o1, Opakowanie<Integer> o2) {
				return o1.compareTo(o2);
			}
			
		}));
		Iterator<Opakowanie<Integer>> it = lista.iterator();
		while (it.hasNext()) {
			System.out.println( it.next() );
		}


	}

}
