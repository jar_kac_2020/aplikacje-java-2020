import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class PorArrayListILinkedList {
	public static void main(String[] args) {
		List<Integer> al = new ArrayList<>(100000); 
		List<Integer> v = new Vector<>(100000); 
		List<Integer> ll = new LinkedList<>();
		zmierzCzasDodawanie( "ArrayList", al );
		zmierzCzasDodawanie( "Vector", v );
		zmierzCzasDodawanie( "LinkedList", ll );
		zmierzCzasDostep( "ArrayList", al );
		zmierzCzasDostep( "Vector", v );
		//zmierzCzasDostep( "LinkedList", ll );
	}

	private static void zmierzCzasDodawanie(String string, List<Integer> l) {
		long t0 = System.currentTimeMillis();
		for (int i=0;i<100000;i++) {
			l.add( i );
		}
		long tk = System.currentTimeMillis();
		System.out.println( string+": "+(tk-t0) );
	}

	private static void zmierzCzasDostep(String string, List<Integer> l) {
		long t0 = System.currentTimeMillis();
		long suma = 0;
		for (int i=0;i<100000;i++) {
			suma += l.get( (i*173)%l.size() );
		}
		long tk = System.currentTimeMillis();
		System.out.println( string+": "+(tk-t0) );
	}
}
