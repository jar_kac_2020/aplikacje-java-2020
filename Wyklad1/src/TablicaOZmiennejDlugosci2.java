
public class TablicaOZmiennejDlugosci2 {
	private int dlugoscTablicy = 0;
	private int dane[] = new int[2];
	private int suma;
	
	public int getLength() {
		return dlugoscTablicy;
	}

	public void add(int i) {
		suma += i;
		if (dlugoscTablicy == dane.length) {
			int temp[] = new int[dane.length*2];
			for (int idx=0;idx<dane.length;idx++)
				temp[idx] = dane[idx];
			dane = temp;
		}
		dane[dlugoscTablicy] = i;
		dlugoscTablicy++;
	}

	public int get(int idx) {
		return dane[idx];
	}

	public int getSum() {
		return suma;
	}

}
