import static org.junit.Assert.*;

import org.junit.Test;

public class TestTablicaOZmiennejDlugosci {
	
	@Test
	public void testUtworzPustaTablice() {
		TablicaOZmiennejDlugosci tablica = new TablicaOZmiennejDlugosci();
		assertEquals(0, tablica.getLength());
	}

	@Test
	public void testDlugoscTablicyPoDodaniuElementow() {
		TablicaOZmiennejDlugosci tablica = new TablicaOZmiennejDlugosci();
		tablica.add( 5 );
		assertEquals(1, tablica.getLength());
		tablica.add( 5 );
		assertEquals(2, tablica.getLength());
	}

	@Test
	public void testElementyZapamietywane() {
		TablicaOZmiennejDlugosci tablica = new TablicaOZmiennejDlugosci();
		tablica.add( 5 );
		tablica.add( 6 );
		assertEquals(5, tablica.get(0) );
		assertEquals(6, tablica.get(1) );
	}

	@Test
	public void testMoznaZapisacDuzoElementow() {
		TablicaOZmiennejDlugosci tablica = new TablicaOZmiennejDlugosci();
		for (int i=0;i<1000;i++)
			tablica.add( 1000-i );
		assertEquals(300, tablica.get(700) );
	}

	@Test
	public void testSumaElementow() {
		TablicaOZmiennejDlugosci tablica = new TablicaOZmiennejDlugosci();
		tablica.add( 1 );
		tablica.add( 2 );
		tablica.add( 9 );
		tablica.add( 8 );
		assertEquals(20, tablica.getSum() );
	}
}
