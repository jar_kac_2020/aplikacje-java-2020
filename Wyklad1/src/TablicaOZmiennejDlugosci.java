import java.util.ArrayList;
import java.util.List;

public class TablicaOZmiennejDlugosci {
	private List<Integer> dane = new ArrayList<>();

	public int getLength() {
		return dane.size();
	}

	public void add(int i) {
		dane.add( Integer.valueOf(i) );
	}

	public int get(int index) {
		return dane.get(index);
	}

	public int getSum() {
		int suma = 0;
		for (Integer e:dane) suma += e;
		return suma;
	}
}
