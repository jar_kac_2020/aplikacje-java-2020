
public class Uruchom {

	public static void main(String[] args) {
		FiguraGeometryczna figuraGeometryczna[] = new FiguraGeometryczna[10];
		figuraGeometryczna[0] = new Kolo();
		figuraGeometryczna[0].ustawBoki( new int[] {10} );
		
		figuraGeometryczna[1] = new Kolo();
		figuraGeometryczna[1].ustawBoki( new int[] {20} );

		figuraGeometryczna[2] = new Kwadrat();
		figuraGeometryczna[2].ustawBoki( new int[] {20} );

		figuraGeometryczna[3] = new FiguraGeometryczna() {
			@Override
			public int dajPole() {
				return -1;
			}
		};
		
		PosiadajacyPole p = new PosiadajacyPole() {

			@Override
			public int dajPole() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public int dajPoleWHektarach() {
				// TODO Auto-generated method stub
				return 0;
			}
			
		};
		
		PosiadajacyPole wszystkie[] = new PosiadajacyPole[10];
		wszystkie[0] = figuraGeometryczna[0];
		wszystkie[1] = new Rolnik();
		
		int sumaWszystkichPol = 0;
		for (PosiadajacyPole f:figuraGeometryczna) {
			sumaWszystkichPol += f.dajPole();
		}
	}

}
