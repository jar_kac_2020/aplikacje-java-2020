
abstract public class FiguraGeometryczna implements PosiadajacyPole {
	public int[] boki;

	public int dajObwod() {
		int suma = 0;
		for (int i=0;i<boki.length;i++) suma += boki[i];
		return suma;
	}

	public void ustawBoki(int[] boki) {
		this.boki = boki;
	} 
	
	abstract public int dajPole();
}
