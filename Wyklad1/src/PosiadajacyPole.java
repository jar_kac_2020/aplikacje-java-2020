
public interface PosiadajacyPole {
	int NAJMNIEJSZE_POLE = 0;
	int dajPole();
	default int dajPoleWHektarach() {
		return dajPole()/10000;
	}
}
